# Rajya Sabha Question Scraper

This is an **AI** \*cough\* solution to read a PDF form Rajya Sabha Question/Answer into structured json form.

## Installation

The file `rs.py` needs to be run with python (>=3). So, a basic python setup is necessary.

It internally depends on [pdftotext](https://www.xpdfreader.com/pdftotext-man.html) to get the text out - so, you would need a linux system (or subsystem) with pdftotext available on the commandline.

"If you wish to make an apple pie from scratch, you must first invent the universe"  ~ Carl Sagan

## Running

* Just download a pdf from [Rajya Sabha website](https://rajyasabha.nic.in/rsnew/Questions/DateSearch.aspx) - only English works for now. Let us say this file is named `AU1234.pdf`
* Run `python rs.py AU1234.pdf`
* See the question and answer in structured form.