import sys
import subprocess
import json

DEBUG=False

def log(message):
    if DEBUG:
        print(message)

def get_text_out_of_pdf(filename):
    completedConversion = subprocess.run(["pdftotext", filename, "-"], capture_output=True, encoding="utf-8")
    return completedConversion.stdout

def clean_line(line):
    return line.strip('.').strip().strip('.').strip()

def is_goi_tag(line):
    return "india" in line.lower()

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def get_ministry_from(line):
    return remove_prefix(line, 'MINISTRY OF ')

def starred_or_unstarred(line):
    if "unstarred" in line.lower():
        return "UNSTARRED"
    else:
        return "STARRED"

def get_qno(line):
    return "".join([char for char in line if char.isdigit()])

def get_date(line, separator = "."):
    datechars = [char for char in line if char.isdigit()]
    day = "".join(datechars[0:2])
    month = "".join(datechars[2:4])
    year = "".join(datechars[4:8])
    return separator.join([day, month, year])

def is_line_essentially_empty(line):
    return line.isspace() or line == '' or clean_line(line) in ["(", "", ":"]

def is_line_only_number(line):
    return line.strip().isdigit()

def is_line_looking_like_question(line):
    return len(line) > 70

def get_asker_from(line):
    return "".join([char for char in line if not char.isdigit()]).strip().strip('.').strip()

def is_line_answer_tag(line):
    return line.lower() == 'answer'

def get_things_line_by_line(text):
    found_goi_tag = False
    question_body_started = False
    answer_body_started = False
    processed = {}
    for line in text.splitlines():
        cleaned_line = clean_line(line)
        log("processing line " + cleaned_line)
        if not found_goi_tag:
            found_goi_tag = is_goi_tag(cleaned_line)
            if found_goi_tag:
                log("Found goi in " + line)
            continue
        if not "ministry" in processed:
            processed["ministry"] = get_ministry_from(cleaned_line)
            log("Found ministry: " + processed["ministry"] + " from line " + line)
            continue
        if not "sabha" in processed:
            processed["sabha"] = "RS"
            continue
        if not "starred" in processed:
            processed["starred"] = starred_or_unstarred(line)
            processed["questionNumber"] = get_qno(cleaned_line)
            continue
        if not "answerDate" in processed:
            processed["answerDate"] = get_date(cleaned_line)
            continue
        if not "questionTitle" in processed:
            processed["questionTitle"] = cleaned_line
            continue
        if not question_body_started:
            if not "asker" in processed:
                processed['asker'] = []
            if is_line_essentially_empty(line):
                question_body_started = True
                continue
            if is_line_looking_like_question(line):
                question_body_started = True
                processed["questionText"] = line
                continue
            if not is_line_only_number(line):
                clean_asker = get_asker_from(cleaned_line)
                if not clean_asker == "":
                    processed['asker'].append(clean_asker)
            continue
        if not answer_body_started:
            if not is_line_answer_tag(cleaned_line):
                if "questionText" not in processed:
                    processed['questionText'] = ""
                processed['questionText'] += line
            else:
                answer_body_started = True
            continue
        else:
            if "answerText" not in processed:
                processed['answerText'] = ""
            processed['answerText'] += line

    return processed



def run(filename):
    text = get_text_out_of_pdf(filename)
    processed = get_things_line_by_line(text)
    return processed

def just_print(filename):
    text = get_text_out_of_pdf(filename)
    return text


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python rs.py [filename] [--debug] [--print]")
        quit()
    if "--debug" in sys.argv:
        DEBUG=True
    filename = sys.argv[1]
    if "--print" in sys.argv:
        print(just_print(filename))
    else:
        print(json.dumps(run(filename), indent=2))
